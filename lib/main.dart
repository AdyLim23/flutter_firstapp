import 'package:flutter/material.dart';
import './product_manager.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
//   @override
//   State<StatefulWidget> createState() {
//     return _MyAppState();
//   }
// }

//_MyAppState extends State<MyApp> means MyAppState <belongs to> MyApp , so the connection between 2 class is se up
// class _MyAppState extends State<MyApp>{
  //List type is the generic type , so it had < >
  //Code below means there is array of the products with String type
  // List<String> _products = ['ProductTester'];
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme:ThemeData(
        primarySwatch: Colors.deepOrange,
        accentColor: Colors.blue,
        brightness: Brightness.dark, 
      ),
      home: Scaffold(    
          appBar: AppBar(
            title: Text('First App')
          ),
          body: 
          // Column(children: [
          ProductManager("Food Tester")  
            //Iterable will require map(() => components).toList()
            
          // ],
          ),
          );
    
  }
}
